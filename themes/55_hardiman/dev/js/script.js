'use strict';

var scrollSpeed = 800;
var $navbar = jQuery('.navbar');

jQuery(document).ready(function ($) {
    OnResize();

    $('.btn-video').scrollTo({
        destination: $('#video'),
        navbar: $navbar
    });
    $('.btn-interiors').scrollTo({
        destination: $('#interiors'),
        navbar: $navbar
    });
    $('.btn-register').scrollTo({
        destination: $('#register'),
        navbar: $navbar
    });

    $('.btn-menu').click(function () {
        $('.menu-nav').toggleClass('active');
    });
    $('.btn-menu-close').click(function () {
        $('.menu-nav').removeClass('active');
    });

    // Carousel section
    var carousels = new Swiper('.carousel-swiper', {
        grabCursor: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    // Split-carousel section
    var splitCarousels = new Swiper('.split-carousel-swiper', {
        grabCursor: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            568: {
                slidesPerView: 2,
                slidesPerGroup: 2,
            },
        },
    });

    // Movie Section
    $('.movie-section-play').click(function () {
        $(this).next('.movie-section-video')[0].play();
        $(this).next('.movie-section-video').addClass('active');
    });

    $('.movie-section-video').on('play', function () {
        $(this).attr('controls', '');
        $(this).prev('.movie-section-play').hide();
    });

    // Set form hidden value
    var parts = document.location.href.split('/');
    var lastSegment = parts.pop() || parts.pop();
    $('input[name=source]').val(lastSegment);

    /// On resize of window ///
    $(window).resize(function () {
        // Call everything that must be done initially and on resize
        OnResize();
    });

    /// On scroll of window ///
    $(window).scroll(function () {
        if ($(window).scrollTop() > 0) {
            $navbar.addClass('coloured');
        } else {
            $navbar.removeClass('coloured');
        }
    });


});

// Initially and on resize
function OnResize() {
}