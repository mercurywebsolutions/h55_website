<?php 
if (is_page_template('template-content.php')) : 
?>
<footer id="register" class="register">
    <div class="register-inner">
        <div class="register-left">
            <?php if ($field = get_field('registration_heading', 'options')) : ?>
                <p class="register-left-heading"><?php echo $field; ?></p>
            <?php endif; ?>
            <?php if ($field = get_field('registration_text', 'options')) : ?>
                <div class="register-left-text text"><?php echo $field; ?></div>
            <?php endif; ?>
        </div>
        <div class="register-right">
            <?php if ($field = get_field('registration_form', 'options')) : ?>
                <?php echo do_shortcode('[contact-form-7 id="' . $field . '" title="' . get_the_title($field) . '"]'); ?>
            <?php endif; ?>
            <?php if (have_rows('footer_logos', 'options')) : ?>
            <p class="footer-logos-heading register-logos-heading">BROUGHT TO YOU BY</p>
            <div class="register-right-logos footer-logos">
                <?php while (have_rows('footer_logos', 'options')) : the_row(); ?>
                    <?php if ($img = get_sub_field('logo')) : ?>
                    <img class="footer-logos-item" src="<?php echo $img; ?>">
                    <?php endif; ?>
                <?php endwhile; ?>
            </div>
            <?php endif; ?>
        </div> 
    </div>
</footer>
<?php endif;?>
<?php wp_footer(); ?>
</body>
</html>