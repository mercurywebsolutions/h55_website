<?php get_header(); ?>

<?php
$hero = get_field('background_image');
$style = 'style="background-image: url(' . $hero . ')"';
?>
<div class="homepage">
    <div class="homepage-left" <?php echo $style; ?>>
        <?php if ($logo = get_field('website_logo', 'options')) : ?>
        <img class="homepage-logo" src="<?php echo $logo; ?>">
        <?php endif; ?>
    </div>
    <div class="homepage-right">
        <?php if ($field = get_field('townhomes_text')) : ?>
        <a class="homepage-heading text" href="<?php echo get_permalink(get_page_by_title('Townhomes')); ?>">
            <?php echo $field; ?>
        </a>
        <?php endif; ?>
        <?php if ($field = get_field('apartments_text')) : ?>
        <a class="homepage-heading text" href="<?php echo get_permalink(get_page_by_title('Apartments')); ?>">
            <?php echo $field; ?>
        </a>
        <?php endif; ?>
        <?php if ($field = get_field('page_subheading')) : ?>
        <div class="homepage-subheading text"><?php echo $field; ?></div>
        <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>