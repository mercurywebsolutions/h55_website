<?php

/************************************************************************
 * function.php:
 * 		- Include all relevant styles & scripts
 * Note: Index files must contain get_header() to enque styles/scripts
 ***********************************************************************/

function load_sources()
{
    $styleURI = get_template_directory_uri() . '/dist/css/styles.css';
    $scriptURI = get_template_directory_uri() . '/dist/js/all.js';

    $style = get_template_directory() . '/dist/css/styles.css';
    $script = get_template_directory() . '/dist/js/all.js';

    wp_enqueue_style('styles', $styleURI, array(), filemtime($style), 'all');

    /* WP's default jquery library seems to be out of date? 
    You cannot manipulate css varlables via jquery without 3.2.2 or heigher */
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.4.1.min.js', array(), null, true);
    wp_enqueue_script('jquery-ui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js', array(), null, true);
    wp_enqueue_script('all', $scriptURI, array('jquery', 'jquery-ui-core', 'jquery-ui-widget'), filemtime($script), true);
}
add_action('wp_enqueue_scripts', 'load_sources');

// remove editor from pages
function remove_content_editor()
{
    remove_post_type_support('page', 'editor');
}
add_action('admin_head', 'remove_content_editor');

// register nav menu locations
function register_custom_nav_menus()
{
    register_nav_menus(array(
        'main_menu' => 'Main Menu',
    ));
}
add_action('after_setup_theme', 'register_custom_nav_menus');

// add featured images to posts
add_theme_support('post-thumbnails');

// add ACF options page
if (function_exists('acf_add_options_page')) {
    acf_add_options_page();
}