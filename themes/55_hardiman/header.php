<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1, user-scalable=0" />
    <title><?php bloginfo('name'); ?></title>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div class="navbar">
        <button type="button" class="btn-menu">
            <img class="hamrburger" src="<?php bloginfo('template_directory'); ?>/dist/image/hamburger.svg">
        </button>
        <h1>55 Hardiman St.</h1>
        <button type="button" class="btn-register btn">
            Register
        </button>
    </div>
    <nav class="menu-nav">
        <button type="button" class="btn-menu-close">
            <img src="<?php bloginfo('template_directory'); ?>/dist/image/close.svg">
        </button>
        <?php wp_nav_menu(array('theme_location' => 'main_menu', 'menu_id' => '', 'menu_class' => 'menu-list', 'container' => false)); ?>
    </nav>