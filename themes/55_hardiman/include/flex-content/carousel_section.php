<section id="interiors" class="carousel-section container">
    <?php if (have_rows('carousel_slides')) : ?>
    <div class="swiper-container carousel-swiper">
        <div class="swiper-wrapper">
            <?php while (have_rows('carousel_slides')) : the_row(); ?>
            <div class="swiper-slide">
                <?php if ($img = get_sub_field('slide_image_mobile')) : ?>
                <img class="carousel-slide-image mobile" src="<?php echo $img; ?>">
                <?php endif; ?>
                <?php if ($img = get_sub_field('slide_image')) : ?>
                <img class="carousel-slide-image desktop" src="<?php echo $img; ?>">
                <?php endif; ?>
            </div>
            <?php endwhile; ?>
        </div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
    <?php endif; ?>
</section>