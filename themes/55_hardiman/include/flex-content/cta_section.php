<section class="cta-section container">
    <?php if ($field = get_sub_field('section_heading')) : ?>
        <div class="cta-heading"> <?php echo $field; ?> </div>
    <?php endif;?>
    <?php if ($field = get_sub_field('section_text')) : ?>
        <div class="cta-text"> <?php echo $field; ?></div>
        <?php endif;?>
        <button class="btn btn-register">Register now</button>
</section>