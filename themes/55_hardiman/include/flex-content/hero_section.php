<?php
$hero = get_sub_field('background_image');
$style = 'style="background-image: url(' . $hero . ')"';
?>
<section class="hero-section" <?php echo $style; ?>>
    <?php if ($logo = get_field('website_logo', 'options')) : ?>
    <img class="hero-logo" src="<?php echo $logo; ?>">
    <?php endif; ?>
</section>