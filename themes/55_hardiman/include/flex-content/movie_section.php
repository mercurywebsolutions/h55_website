<section id="video" class="movie-section container">
    <div class="movie-section-wrapper">
        <?php if ($field = get_sub_field('movie_link')) : ?>
            <button class="movie-section-play" type="button">
                <img src="<?php bloginfo('template_directory'); ?>/dist/image/play-btn.svg">
            </button>
            <video class="movie-section-video" controlsList="nodownload noremoteplayback" poster="<?php echo get_sub_field('movie_poster') ?>">
                <source type="video/mp4" src="<?php echo $field ?>" />
            </video>
        <?php endif; ?>
    </div>
</section>