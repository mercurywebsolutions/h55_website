<section class="split-carousel container">
    <?php if (have_rows('carousel_slides')) : ?>
    <div class="swiper-container split-carousel-swiper">
        <div class="swiper-wrapper">
            <?php while (have_rows('carousel_slides')) : the_row(); ?>
            <div class="swiper-slide split-carousel-slide">
                <?php if (get_sub_field('slide_type') == 'text') : ?>
                    <?php if ($field = get_sub_field('slide_text')) : ?>
                    <div class="split-carousel-text text">
                        <?php echo $field; ?>
                    </div>
                    <?php endif; ?>
                    <?php if ($field = get_sub_field('quote_title')) : ?>
                    <p class="split-carousel-quote"><?php echo $field; ?></p>
                    <?php endif; ?>
                    <?php if ($field = get_sub_field('quote_subtitle')) : ?>
                    <p class="split-carousel-quote-subtitle"><?php echo $field; ?></p>
                    <?php endif; ?>
                <?php elseif (get_sub_field('slide_type') == 'image') : ?>
                    <?php if ($img = get_sub_field('slide_image')) : ?>
                    <img class="split-carousel-image" src="<?php echo $img; ?>">
                    <?php endif; ?>
                <?php endif; ?>
            </div>
            <?php endwhile; ?>
        </div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
    <?php endif; ?>
</section>