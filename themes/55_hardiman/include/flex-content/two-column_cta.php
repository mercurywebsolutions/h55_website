<?php
    $classes = "";
?>

<section class="split-cta-section container">
    <?php
        if(get_sub_field('image_on_right')) {
            $classes = "image-right";
        }
     ?>
    <div class="split-cta-inner <?php echo $classes; ?>">
        <div class="split-cta-top">
            <?php if ($field = get_sub_field('section_text')) : ?>
                <div class="split-cta-top-text"> <?php echo $field; ?> </div>
            <?php endif;?>
            
            <?php if ($field = get_sub_field('quote_title')) : ?>
                <div class="split-cta-top-quote">
                    <div class="split-cta-top-quote-title"><?php echo $field; ?></div>
                    <?php if ($field = get_sub_field('quote_subtitle')) : ?>
                        <div class="split-cta-top-quote-subtitle"><?php echo $field; ?></div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <button class="split-cta-top-btn btn btn-register">Register now</button>
        </div>
        <div class="split-cta-bottom">
        <?php if ($field = get_sub_field('section_image')) : ?>
            <img class="split-cta-bottom-image" src="<?php echo $field; ?>"> 
            <?php endif;?>
        </div>
    </div>
</section>