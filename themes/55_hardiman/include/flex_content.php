<?php
$layouts = array (
    'hero_section',
    'carousel_section',
    'cta_section',
    'two-column_carousel',
    'movie_section',
    'two-column_cta',
);

if (have_rows('flex_content_layouts')) {
    while (have_rows('flex_content_layouts')) {
        the_row();
        foreach ($layouts as $layout) {
            if (get_row_layout() == $layout) {
                include __DIR__ . '/flex-content/' . $layout . '.php';
            }
        }
    }
}