<?php get_header(); ?>

<div class="thankyou">
    <?php if ($field = get_field('page_heading')) : ?>
    <p class="thankyou-heading"><?php echo $field; ?></p>
    <?php endif; ?>
    <?php if ($field = get_field('page_text')) : ?>
    <div class="thankyou-text text"><?php echo $field; ?></div>
    <?php endif; ?>
    <?php
    $pdf = '';
    if ($_GET['source'] == 'townhomes') {
        $pdf = 'Townhomes';
    } elseif ($_GET['source'] == 'apartments') {
        $pdf = 'Apartments';
    }
    ?>
    <a href="<?php bloginfo('template_directory'); ?>/dist/files/Brochure_<?php echo $pdf; ?>.pdf" class="btn-brochure" target="_blank">
        <p>Download brochure</p>
        <img class="btn-brochure-image" src="<?php bloginfo('template_directory'); ?>/dist/image/pdf.svg">
    </a>
    <a class="btn back-to-home" href="<?php echo get_site_url(); ?>">Back to home</a>
    <?php if (have_rows('footer_logos', 'options')) : ?>
    <p class="footer-logos-heading">BROUGHT TO YOU BY</p>
    <div class="footer-logos">
        <?php while (have_rows('footer_logos', 'options')) : the_row(); ?>
            <?php if ($img = get_sub_field('logo')) : ?>
            <img class="footer-logos-item" src="<?php echo $img; ?>">
            <?php endif; ?>
        <?php endwhile; ?>
    </div>
    <?php endif; ?>
</div>

<!-- Google Code for RegOnline Confirmation Conversion Page -->
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="https://www.googleadservices.com/pagead/conversion/818183125/?value=1&amp;label=Gu0ACNmb-nsQ1feRhgM"/>
</div>
</noscript>

<!-- ta facebook lead pixel -->
<img src="https://www.facebook.com/tr?id=178231955890916&ev=Lead&noscript=1" height="1" width="1" />

<?php get_footer(); ?>